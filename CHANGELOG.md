# ChangeLog

## Unreleased

## 4.0.3 (2024-11-20)

**Fixed:**
* Cache event listener value class

## 4.0.2 (2024-06-08)

**Changed:**
* New connector with improved run API

## 4.0.1 (2024-04-22)

**Fixed:**
* Exception handling

## 4.0.0 (2024-04-21)

**Added:**
* Predefined and provisioned dcat-ap 3.0.0 JSON LD context (fixed one)
* Convenient helper functions for scripting
* Cache listener for releasing script engine resources
* Possibility to transform also from String to String and not only from JSON to JSON (depending on mimetype)
* SCRIPTING.md for starter scripting guide lines 
* Mode for debugging scripts

**Changed:**
* Using GraalVM `Context` instead of `ScriptEngine`
* Refactoring code for better testing abilities

## 3.2.3 (2023-11-03)

**Added:**
* Locale ENV for the GraalVM to handle UTF8 properly

## 3.2.2 (2023-10-30)

**Changed:**
* Dependencies update
* Log style

## 3.2.1 (2023-06-07)

**Changed:**
* Use worker verticle for transformations

## 3.2.0 (2023-04-10)

**Fixed:**
* In README.md, `path` instead of `script` for `localFile` case  

**Added:**
* helm chart

**Changed:**
* Bump up lib versions

## 3.1.0 (2022-10-17)

**Changed:**
* Event loop for transforming verticle
* One instance for transforming verticle

**Fixed:**
* Interaction between cache and future improved

## 3.0.10 (2022-10-16)

**Removed:**
* Debris trace log output

**Added:**
* Explicit installing GraalVM java script engine package in docker image

## 3.0.9 (2022-10-16)

**Changed:**
* Default `single` config value to true

**Added:**
* More trace log output

## 3.0.8 (2022-10-16)

**Added:**
* Enrich transforming with a lot of trace output

## 3.0.7 (2022-10-10)

**Changed:**
* Lib updates

## 3.0.6 (2022-09-01)

**Changed:**
* Lib updates

**Fixed:**
* Script loading from pipe config

## 3.0.5 (2022-05-12)

**Changed:**
* Make repository verticle an event loop verticle 
* Decrease cache dimension

## 3.0.4 (2022-04-22)

**Added:**
* Repository verticle to avoid conflicting git repo actions 

**Changed:**
* Separate cache for each verticle instance 
* Better asynchronous processing of pipes

## 3.0.3 (2022-04-19)

**Changed:**
* GraalVM version

## 3.0.2 (2021-10-19)

**Changed**
* Use [GraalVM JavaScript](https://www.graalvm.org/reference-manual/js/) per library
* Important connector lib update

**Fixed**
* Loading script from local file

## 3.0.1 (2021-06-24)

**Changed:**
* Connector pipe handling

## 3.0.0 (2021-06-05)

**Removed:**
* RDF normalization

## 2.1.1 (2021-05-06)

**Changed**
* Bump piveau-utils version

## 2.1.0 (2021-01-27)

**Added:**
* Log transformation result as debug output
* Use script from local file system (`scriptType: localFile`)

**Changed:**
* GraalVM image source and version updated
* Switched to Vert.x 4

## 2.0.0 (2020-09-03)

**Changed:**
* Allow also pure json forwarding (outputFormat="application/json")

**Removed:**
* Saxon-HE dependency

## 1.0.4 (2020-05-27)

**Fixed:**
* Forwarding bad pipe

## 1.0.3 (2020-05-27)

**Changed:**
* Container uses GraalVM for better JavaScript engine

## 1.0.2 (2020-03-05)

**Fixed:**
* Read config params as json object

## 1.0.1 (2020-02-28)

**Changed:**
* Update to new connector lib

**Removed:**
* Explicit web client, using connectors own client

## 1.0.0 (2019-11-08)

**Added:**
* Configuration change listener
* `PIVEAU_LOG_LEVEL` for general log level configuration of the `io.piveau`package

**Changed:**
* Requires now latest LTS Java 11
* Docker base image to openjdk:11-jre

**Removed:**
* Hash calculation removed

**Fixed:**
* Update all dependencies

## 0.1.1 (2019-07-12)

**Added:**
* buildInfo.json for build info via `/health` path
* Environment variable for repository default branch
* config.schema.json

**Changed:**
* Readme
* Add `PIVEAU_` prefix to logstash configuration environment variables

## 0.1.0 (2019-05-17)

**Added:**
* Environment variable for repository default branch

**Changed:**
* Readme

## 0.0.1 (2019-05-03)

Initial release

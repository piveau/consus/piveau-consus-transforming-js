package io.piveau.transforming.js;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Value;

public class PiveauScriptEngine {

    private final Context context;

    private static final String JSON_TRANSFORMING_FUNCTION = "(function(obj) { return JSON.stringify(transforming(JSON.parse(obj)), null, \"  \") })";
    private static final String STRING_TRANSFORMING_FUNCTION = "(function(obj) { return transforming(obj) })";

    public static PiveauScriptEngine create(String script, String piveauScript, JsonObject inspect) {
        Context.Builder builder = Context.newBuilder("js");
        if (inspect != null) {
            builder
                    .allowAllAccess(true)
                    .option("inspect", inspect.getString("port", "4242"))
                    .option("inspect.Path", inspect.getString("path", "debug"))
                    .option("inspect.Suspend", inspect.getString("suspend", "false"))
                    .option("inspect.WaitAttached", inspect.getString("waitAttached", "true"));
        }
        Context context = builder.build();

        context.eval("js", piveauScript);
        context.eval("js", script);

        return new PiveauScriptEngine(context);
    }

    public static PiveauScriptEngine create(String script, String piveauScript, boolean debuggingEnabled) {
        JsonObject inspect = debuggingEnabled ? new JsonObject() : null;
        return create(script, piveauScript, inspect);
    }

    public static PiveauScriptEngine create(String script, String piveauScript, JsonObject dcatapContext, boolean debuggingEnabled) {
        PiveauScriptEngine engine = create(script, piveauScript, debuggingEnabled);
        engine.addContext(dcatapContext);
        return engine;
    }

    public static Future<PiveauScriptEngine> create(Vertx vertx, String script) {
        Future<JsonObject> contextFuture = vertx.fileSystem().readFile("js/dcat-ap.jsonld").map(Buffer::toJsonObject);
        Future<String> piveauFuture = vertx.fileSystem().readFile("js/piveau.js").map(Buffer::toString);
        return Future.join(piveauFuture, contextFuture)
                .map(v -> create(script, piveauFuture.result(), contextFuture.result(), false));
    }

    private PiveauScriptEngine(Context context) {
        this.context = context;
    }

    private void addContext(JsonObject dcatapContext) {
        context.eval("js", "const dcatapContext = " + dcatapContext.encode() + ";");
    }

    public void addParams(JsonObject params) {
        context.eval("js", "const params = " + params.encode() + ";");
    }

    public String invokeTransformation(String data, String mimeType) {
        Value value = context.eval("js", mimeTypeToFunction(mimeType));
        return value.execute(data).asString();
    }

    private String mimeTypeToFunction(String mimeType) {
        if (mimeType != null && !mimeType.isBlank()) {
            if (mimeType.equals("application/ld+json") || mimeType.equals("application/json")) {
                return JSON_TRANSFORMING_FUNCTION;
            } else {
                return STRING_TRANSFORMING_FUNCTION;
            }
        } else {
            return JSON_TRANSFORMING_FUNCTION;
        }
    }

    public void close() {
        context.close();
    }
}

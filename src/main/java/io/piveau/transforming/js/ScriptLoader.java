package io.piveau.transforming.js;

import io.piveau.transforming.repositories.RepositoryVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;

public class ScriptLoader {

    private final Vertx vertx;

    public ScriptLoader(Vertx vertx) {
        this.vertx = vertx;
    }

    public Future<String> getScript(JsonObject config) {
        return Future.future(promise -> {
            switch (config.getString("scriptType", "")) {
                case "repository" -> {
                    JsonObject repository = config.getJsonObject("repository");
                    vertx.eventBus().<String>request(RepositoryVerticle.ADDRESS, repository)
                            .onSuccess(reply -> promise.complete(reply.body()))
                            .onFailure(promise::fail);
                }
                case "localFile" -> {
                    String path = "scripts/" + config.getString("path", "default.js");
                    vertx.fileSystem().exists(path)
                            .compose(exists -> {
                                if (Boolean.TRUE.equals(exists)) {
                                    return vertx.fileSystem().readFile(path).map(Buffer::toString);
                                } else {
                                    return Future.failedFuture("Script " + path + " not found");
                                }
                            }).onComplete(promise);
                }
                case "embedded" -> {
                    String script = config.getString("script", "");
                    if (!script.isBlank()) {
                        promise.complete(script);
                    } else {
                        promise.fail("Invalid embedded script");
                    }
                }
                default -> promise.fail("Unknown script type");
            }

        });
    }

    public Future<String> loadResource(String resource) {
        return vertx.fileSystem().readFile(resource).map(Buffer::toString);
    }

}

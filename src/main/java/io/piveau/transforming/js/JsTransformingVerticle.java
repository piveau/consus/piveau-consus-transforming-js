package io.piveau.transforming.js;

import io.piveau.pipe.PipeContext;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.ehcache.Cache;
import org.ehcache.config.builders.*;
import org.ehcache.config.units.EntryUnit;
import org.ehcache.event.EventType;

import java.time.Duration;
import java.util.concurrent.Executors;

public class JsTransformingVerticle extends AbstractVerticle {

    public static final String ADDRESS = "io.piveau.pipe.transformation.js.queue";

    private Cache<String, Future> cache;

    private JsonObject dcatapContext = new JsonObject();
    private String piveauScript = "";

    private ScriptLoader scriptLoader;

    @Override
    public void start(Promise<Void> startPromise) {
        vertx.eventBus().consumer(ADDRESS, this::handlePipe);

        cache = UserManagedCacheBuilder.newUserManagedCacheBuilder(String.class, Future.class)
                .withResourcePools(
                        ResourcePoolsBuilder.newResourcePoolsBuilder().heap(100, EntryUnit.ENTRIES))
                .withEventExecutors(
                        Executors.newSingleThreadExecutor(),
                        Executors.newFixedThreadPool(5))
                .withEventListeners(
                        CacheEventListenerConfigurationBuilder
                                .newEventListenerConfiguration(new ExpiringListener(), EventType.EXPIRED, EventType.EVICTED)
                                .asynchronous()
                                .unordered())
                .withExpiry(
                        ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofHours(2)))
                .build(true);

        scriptLoader = new ScriptLoader(vertx);

        Future<JsonObject> contextFuture = vertx.fileSystem().readFile("js/dcat-ap.jsonld")
                .map(Buffer::toJsonObject);
        Future<String> scriptFuture = vertx.fileSystem().readFile("js/piveau.js")
                .map(Buffer::toString);

        Future.join(contextFuture, scriptFuture)
                .onComplete(ar -> {
                    if (contextFuture.succeeded()) {
                        dcatapContext = contextFuture.result();
                    }
                    if (scriptFuture.succeeded()) {
                        piveauScript = scriptFuture.result();
                    }
                    startPromise.complete();
                });
    }

    private void handlePipe(Message<PipeContext> message) {
        PipeContext pipeContext = message.body();
        pipeContext.log().trace("Incoming pipe");
        JsonObject config = pipeContext.getConfig();

        JsonObject dataInfo = pipeContext.getDataInfo();

        String content = dataInfo.getString("content", "metadata");

        if (content.equals("metadata")) {

            String runId = pipeContext.getPipe().getHeader().getRunId();

            getOrCreateScriptEngine(runId, config)
                    .onSuccess(engine -> {

                        String data = pipeContext.getStringData();

                        if (pipeContext.log().isDebugEnabled()) {
                            pipeContext.log().debug("Transformation input:\n{}", data);
                        }

                        try {
                            String output = engine.invokeTransformation(data, pipeContext.getMimeType());

                            if (pipeContext.log().isDebugEnabled()) {
                                pipeContext.log().debug("Transformation result:\n{}", output);
                            }

                            String outputFormat = config.getString("outputFormat", "application/ld+json");
                            pipeContext.setResult(output, outputFormat, dataInfo).forward();
                            pipeContext.log().info("Transformation finished: {}", dataInfo);
                        } catch (Exception e) {
                            pipeContext.log().error("Transformation failed: {}", dataInfo.encode(), e);
                        }

                    })
                    .onFailure(pipeContext::setFailure);
        } else {
            pipeContext.log().trace("Passing pipe");
            pipeContext.pass();
        }
    }

    private Future<PiveauScriptEngine> getOrCreateScriptEngine(String runId, JsonObject config) {
        boolean useCache = config.getBoolean("single", true);
        if (useCache && cache.containsKey(runId)) {
            return cache.get(runId);
        } else {
            return Future.future(promise -> {
                if (useCache) {
                    cache.put(runId, promise.future());
                }
                scriptLoader.getScript(config)
                        .onSuccess(script -> {
                            PiveauScriptEngine engine = PiveauScriptEngine.create(script, piveauScript, dcatapContext, false);
                            engine.addParams(config.getJsonObject("params", new JsonObject()));
                            promise.complete(engine);
                        })
                        .onFailure(promise::fail);
            });
        }
    }

}

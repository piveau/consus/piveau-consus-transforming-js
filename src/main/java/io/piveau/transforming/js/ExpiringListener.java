package io.piveau.transforming.js;

import io.vertx.core.Future;
import org.ehcache.event.CacheEvent;
import org.ehcache.event.CacheEventListener;
import org.ehcache.event.EventType;

import java.util.List;

public class ExpiringListener implements CacheEventListener<String, Future<PiveauScriptEngine>> {
    List<EventType> events = List.of(EventType.EXPIRED, EventType.EVICTED);
    @Override
    public void onEvent(CacheEvent<? extends String, ? extends Future<PiveauScriptEngine>> cacheEvent) {
        if (events.contains(cacheEvent.getType())) {
            Future<PiveauScriptEngine> future = cacheEvent.getOldValue();
            if (future.succeeded()) {
                future.result().close();
            }
        }
    }
}


package io.piveau.transforming.repositories;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.EntryUnit;

import java.nio.file.Path;
import java.time.Duration;

public class RepositoryVerticle extends AbstractVerticle {

    public static final String ADDRESS = "io.piveau.pipe.transformation.repo.queue";

    private Cache<String, Future> cache;

    @Override
    public void start(Promise<Void> startPromise) {
        vertx.eventBus().consumer(ADDRESS, this::handleMessage);
        CacheManager cacheManager = CacheManagerBuilder.newCacheManagerBuilder()
                .withCache("repo-verticle", CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, Future.class,
                                ResourcePoolsBuilder.newResourcePoolsBuilder().heap(200, EntryUnit.ENTRIES))
                        .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofMinutes(30))))
                .build(true);
        cache = cacheManager.getCache("repo-verticle", String.class, Future.class);

        startPromise.complete();
    }

    private void handleMessage(Message<JsonObject> message) {
        JsonObject repository = message.body();
        String uri = repository.getString("uri");
        String branch = repository.getString("branch", "master");
        String username = repository.getString("username");
        String token = repository.getString("token");

        String path = GitRepository.createPath(uri, branch).toString();
        Future<GitRepository> future;
        if (cache.containsKey(path)) {
            future = cache.get(path);
        } else {
            future = vertx.executeBlocking(promise -> {
                GitRepository gitRepo = GitRepository.open(uri, username, token, branch);
                promise.complete(gitRepo);
            });
            cache.put(path, future);
        }

        future.onSuccess(repo -> {
            try {
                Path file = repo.resolve(repository.getString("script"));
                vertx.fileSystem().readFile(file.toString())
                        .onSuccess(buffer -> message.reply(buffer.toString()))
                        .onFailure(cause -> message.fail(-1, cause.getMessage()));
            } catch (Exception e) {
                message.fail(-2, e.getMessage());
            }
        }).onFailure(cause -> message.fail(-3, cause.getMessage()));
   }

}
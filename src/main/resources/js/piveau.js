var piveau = piveau || {};

piveau.executeJsonTransformation = (obj) => {
    return transforming(JSON.parse(obj))
}
const piveau_executeJsonTransformation = piveau.executeJsonTransformation

piveau.executeStringTransformation = (obj) => {
    return transforming(obj)
}
const piveau_executeStringTransformation = piveau.executeStringTransformation

piveau.mapLiteral = (literal, target, lang) => {
    if (literal) {
        if (target === undefined) {
            target = []
        }
        let lit = {
            "@value": literal
        }
        if (lang) {
            lit["@language"] = lang;
        }
        target.push(lit)
        return target;
    }
}

piveau.mapSingleLiteral = (literal, lang) => {
    if (literal) {
        let target = {
            "@value": literal
        }
        if (lang) {
            target["@language"] = lang;
        }
        return target;
    }
}
const mapSingleLiteral = piveau.mapSingleLiteral

piveau.mapTypedLiteral = (literal, target, type) => {
    if (literal) {
        if (target === undefined) {
            target = []
        }
        let lit = {
            "@value": literal
        }
        if (type) {
            lit["@type"] = type;
        }
        target.push(lit)
        return target;
    }
}

piveau.mapSingleTypedLiteral = (literal, type) => {
    if (literal) {
        let target = {
            "@value": literal
        }
        if (type) {
            target["@type"] = type;
        }
        return target;
    }
}

piveau.mapReference = (reference, target, type) => {
    if (reference) {
        if (target === undefined) {
            target = []
        }
        let ref = {
            "@id": encodeURI(decodeURI(reference)),
        }
        if (type) {
            ref["@type"] = type;
        }
        target.push(ref)
        return target;
    }
}

piveau.mapSingleReference = (reference, type) => {
    if (reference) {
        let target = {
            "@id": encodeURI(decodeURI(reference)),
        }
        if (type) {
            target["@type"] = type;
        }
        return target;
    }
}

function transforming(input) {

    let output = dcatapContext;

    // Basics

    output["@type"] = "Dataset";

    // Mandatory properties

    output.title = piveau.mapLiteral(input.title, output.title, params.defaultLanguage)
    output.description = piveau.mapLiteral(input.notes, output.description, params.defaultLanguage)

    // Recommended properties

    output.page = piveau.mapReference(input.url, output.page, "Document")

    for (const resource of input.resources||[]) {
        if (resource.access_url) {
            let dist = {
                "@type": "Distribution",
                "identifier": resource.id,
                "accessUrl": [{
                    "@id": encodeURI(decodeURI(resource.access_url))
                }],
                "description": [{
                    "@value": resource.description,
                    "@language": params.defaultLanguage
                }]
            };
            dist.downloadUrl = piveau.mapReference(resource.download_url, dist.downloadURL)
            dist.title = piveau.mapLiteral(resource.name, dist.title, params.defaultLanguage)
            dist.format = piveau.mapSingleReference(resource.format)

            dist.licence = piveau.mapSingleReference(resource.license, "LicenceDocument")
            if (dist.licence) {
                dist.licence.title = piveau.mapLiteral(resource.license_infos, dist.licence.title)
            }

            // todo Map simple string value to rdf uriref
            dist.mediaType = piveau.mapSingleLiteral(resource.mimetype, dist.mediaType)

            dist.byteSize = piveau.mapSingleTypedLiteral(resource.size, "xsd:nonNegativeInteger")

            for (const page of JSON.parse(resource.documentation||[])) {
                dist.page = piveau.mapReference(page, dist.page);
            }
            for (const lang of JSON.parse(resource.language||[])) {
                dist.language = piveau.mapReference(lang, dist.language);
            }
            dist.releaseDate = piveau.mapSingleTypedLiteral(resource.issued, "xsd:dateTime");
            dist.modificationDate = piveau.mapSingleTypedLiteral(resource.modified, "xsd:dateTime");
            dist.rights = piveau.mapSingleReference(resource.rights, "RightsStatement");
            dist.status = piveau.mapSingleReference(resource.status);

            output.datasetDistribution = output.datasetDistribution || [];
            output.datasetDistribution.push(dist);
        }
    }

    for (const tag of input.tags||[]) {
        output.keyword = piveau.mapLiteral(tag.name, output.keyword, params.defaultLanguage)
    }

    // Extras

    for (const extra of input.extras||[]) {
        if (extra.value === "") continue;
        switch (extra.key) {
            case "access_rights":
                switch(extra.value.trim()) {
                    case "öffentlich":
                        output.accessRights = {
                            "@id": "http://publications.europa.eu/resource/authority/access-right/PUBLIC"
                        };
                        break;
                    case "nicht öffentlich":
                        output.accessRights = {
                            "@id": "http://publications.europa.eu/resource/authority/access-right/NON_PUBLIC"
                        };
                        break;
                    case "vertraulich":
                        output.accessRights = {
                            "@id": "http://publications.europa.eu/resource/authority/access-right/CONFIDENTIAL"
                        };
                        break;
                    case "eingeschränkt":
                        output.accessRights = {
                            "@id": "http://publications.europa.eu/resource/authority/access-right/RESTRICTED"
                        };
                        break;
                    case "sensibel":
                        output.accessRights = {
                            "@id": "http://publications.europa.eu/resource/authority/access-right/SENSITIVE"
                        };
                        break;
                }
                break;
            case "contact_email":
                if (output.contactPoint === undefined) {
                    output.contactPoint = [{
                        "@type": "Kind"
                    }];
                }
                output.contactPoint[0]["http://www.w3.org/2006/vcard/ns#hasEmail"] = { "@id": "mailto:" + extra.value };
                break;
            case "contact_name":
                if (output.contactPoint === undefined) {
                    output.contactPoint = [{
                        "@type": "Kind"
                    }];
                }
                output.contactPoint[0]["http://www.w3.org/2006/vcard/ns#fn"] = extra.value;
                break;
            case "publisher_name":
                if (output.publisher === undefined) {
                    output.publisher = {
                        "@type": "Agent"
                    };
                }
                output.publisher.name = extra.value;
                break;
            case "publisher_url":
                if (output.publisher === undefined) {
                    output.publisher = {
                        "@type": "Agent"
                    };
                }
                output.publisher["http://xmlns.com/foaf/0.1/homepage"] = { "@id": extra.value };
                break;
            case "documentation":
                for (const page of JSON.parse(extra.value)||[]) {
                    output.page = piveau.mapReference(page, output.page)
                }
                break;
            case "uri":
                output.landingPage = piveau.mapReference(extra.value, output.landingPage);
                break;
            case "frequency":
                output.accrualPeriodicity = piveau.mapSingleReference(extra.value);
                break;
            case "identifier":
                output.identifier = extra.value;
                break;
            case "issued":
                output.releaseDate = piveau.mapSingleTypedLiteral(extra.value, "xsd:dateTime");
                break;
            case "modified":
                output.modificationDate = piveau.mapSingleTypedLiteral(extra.value, "xsd:dateTime");
                break;
            case "language":
                for (const lang of JSON.parse(extra.value)||[]) {
                    output.language = piveau.mapReference(lang, output.language);
                }
                break;
            case "spatial_uri":
                output.spatial = piveau.mapReference(extra.value, output.spatial);
                break;
            case "theme":
                for (const theme of JSON.parse(extra.value)||[]) {
                    output.theme = piveau.mapReference(theme, output.theme);
                }
                break;
            case "temporal_start":
                if (!output.temporal) {
                    output.temporal = [{}];
                }
                output.temporal[0].startDate = piveau.mapSingleTypedLiteral(extra.value,"xsd:dateTime");
                break;
            case "temporal_end":
                if (!output.temporal) {
                    output.temporal = [{}];
                }
                output.temporal[0].endDate = piveau.mapSingleTypedLiteral(extra.value, "xsd:dateTime");
                break;
        }
    }

    // Optional properties

    // Finalize

    return output;
}

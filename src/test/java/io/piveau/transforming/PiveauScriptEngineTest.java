package io.piveau.transforming;

import io.piveau.rdf.Piveau;
import io.piveau.transforming.js.PiveauScriptEngine;
import io.piveau.transforming.js.ScriptLoader;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@DisplayName("Testing script engine")
@ExtendWith(VertxExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PiveauScriptEngineTest {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private ScriptLoader scriptLoader;

    @BeforeAll
    void beforeAll(Vertx vertx, VertxTestContext testContext) {
        scriptLoader = new ScriptLoader(vertx);
        testContext.completeNow();
    }

    @Test
    void testScriptEngine() {
        PiveauScriptEngine engine = PiveauScriptEngine.create("42", "let test = \"piveau\";", false);
        Assertions.assertNotNull(engine);
        engine.close();
    }

    @Test
    void testScriptEngineWithDebugging() {
        JsonObject inspect = new JsonObject()
                .put("waitAttached", "false");
        PiveauScriptEngine engine = PiveauScriptEngine.create("42", "let test = \"piveau\";", inspect);
        Assertions.assertNotNull(engine);
        engine.close();
    }

    @Test
    void testScript(Vertx vertx, VertxTestContext testContext) {

        Future<PiveauScriptEngine> engineFuture = scriptLoader.loadResource("script.js")
                .compose(script -> PiveauScriptEngine.create(vertx, script));

        Future<JsonObject> inputFuture = vertx.fileSystem().readFile("dataset.json").map(Buffer::toJsonObject);

        Future.join(engineFuture, inputFuture)
                .onSuccess(v -> {
                    PiveauScriptEngine engine = engineFuture.result();
                    JsonObject params = new JsonObject().put("defaultLanguage", "de");
                    engine.addParams(params);

                    JsonObject input = inputFuture.result();

                    try {

                        String output = engine.invokeTransformation(input.encode(), "application/json");
                        log.debug(output);

                        testContext.completeNow();
                    } catch (Exception e) {
                        testContext.failNow(e);
                    }

                    engine.close();
                })
                .onFailure(testContext::failNow);
    }

    @Test
    @Disabled("This is for debugging purposes only")
    void debugScript() {
        final String scriptResource = "script2.js";
        final String inputResource = "dataset2.json";

        Vertx vertx = Vertx.vertx();

        JsonObject context = vertx.fileSystem().readFileBlocking("js/dcat-ap.jsonld").toJsonObject();
        String piveau = vertx.fileSystem().readFileBlocking("js/piveau.js").toString();

        String script = vertx.fileSystem().readFileBlocking(scriptResource).toString();
        String input = vertx.fileSystem().readFileBlocking(inputResource).toString();

        PiveauScriptEngine engine = PiveauScriptEngine.create(script, piveau, context, true);
        JsonObject params = new JsonObject().put("defaultLanguage", "de");
        engine.addParams(params);

        String output = engine.invokeTransformation(input, "application/json");
        Model model = Piveau.toModel(output.getBytes(), Lang.JSONLD);
        log.debug(Piveau.presentAs(model, Lang.TURTLE));

        Assertions.assertNotNull(output);
    }

}

package io.piveau.transforming;

import io.piveau.transforming.js.ScriptLoader;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(VertxExtension.class)
class ScriptLoaderTest {

    @Test
    void testEmbedded(Vertx vertx, VertxTestContext testContext) {
        new ScriptLoader(vertx).getScript(new JsonObject()
                        .put("scriptType", "embedded")
                        .put("script", "test"))
                .onSuccess(script -> {
                    testContext.verify(() -> {
                        Assertions.assertEquals("test", script);
                        testContext.completeNow();
                    });
                })
                .onFailure(testContext::failNow);
    }

    @Test
    void testLocalFile(Vertx vertx, VertxTestContext testContext) {
        new ScriptLoader(vertx).getScript(new JsonObject()
                        .put("scriptType", "localFile")
                        .put("path", "tests/default.js"))
                .onSuccess(script -> {
                    testContext.verify(() -> {
                        Assertions.assertEquals("test", script);
                        testContext.completeNow();
                    });
                })
                .onFailure(testContext::failNow);
    }

    @Test
    void testUnknownScriptType(Vertx vertx, VertxTestContext testContext) {
        new ScriptLoader(vertx).getScript(new JsonObject()
                        .put("scriptType", "blabla"))
                .onSuccess(script -> testContext.failNow("Script type blabla is known"))
                .onFailure(cause -> {
                    testContext.verify(() -> {
                        Assertions.assertEquals("Unknown script type", cause.getMessage());
                        testContext.completeNow();
                    });
                });
    }

    @Test
    void testLocalFileNotFound(Vertx vertx, VertxTestContext testContext) {
        new ScriptLoader(vertx).getScript(new JsonObject()
                        .put("scriptType", "localFile")
                        .put("path", "tests/blabla.js"))
                .onSuccess(script -> testContext.failNow("Script tests/blabla.js exists"))
                .onFailure(cause -> {
                    testContext.verify(() -> {
                        Assertions.assertEquals("Script scripts/tests/blabla.js not found", cause.getMessage());
                        testContext.completeNow();
                    });
                });
    }

    @Test
    void testInvalidEmbeddedScript(Vertx vertx, VertxTestContext testContext) {
        new ScriptLoader(vertx).getScript(new JsonObject()
                        .put("scriptType", "embedded")
                        .put("script", ""))
                .onSuccess(script -> testContext.failNow("Empty script is valid"))
                .onFailure(cause -> {
                    testContext.verify(() -> {
                        Assertions.assertEquals("Invalid embedded script", cause.getMessage());
                        testContext.completeNow();
                    });
                });
    }

}

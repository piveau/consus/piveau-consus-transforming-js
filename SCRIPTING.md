# Writing scripts for this transformation module

## Table of Contents
1. [Entry Point](#entry-point)
1. [Parameters from the Pipe](#parameters-from-the-pipe)
1. [Convenient Helpers](#convenient-helpers)
   1. [DCAT-AP Context Object(s)](#dcat-ap-context-objects)
   1. [Mapping Functions](#mapping-functions)
1. [Debugging Scripts](#debugging-scripts)

## Entry Point
Your script requires one global function with the following name and signature:

```javascript
function transforming(input) {
    
}
```

`input` is the data content from the pipe payload, usually passed as JSON object or array (default).

## Parameters from the Pipe

The property `params` JSON object of the pipe segment configuration will be passed to the script under the same name.

```javascript
let language = params.defaultLanguage
```

## Convenient Helpers

There are some predefined helper objects and functions that you can use in your scripts.

See [script2.js](src/test/resources/script2.js) for example usage.

if you're working in a DCAT-AP context, you should consider using `mapSingle*` for all properties with a cardinality of 
one and for properties with cardinality greater all other helper functions.

### DCAT-AP Context Object(s)

The module provides an object that just includes a JSON-LD DCAT-AP `@context` object that you can use as starting point
for your generating code.

* `dcatapContext` -> DCAT-AP 3.0.0 JSON LD context definitions (slightly fixed)
  ```javascript
  function transfomring(input) {
    let output = dcatapContext;
    ...    
  }
  ```

### Mapping Functions

* `piveau.mapLiteral(source, target, lang)`
  
  Returning a JSON-LD literal with the value of source and optionally a language attribute. If target exist, it will 
  add it as an additional triple (returning an array, which includes all previous values). If source is undefined, null 
  or blank, the method returns `undefined`.
  ```javascript
  output.title = piveau.mapLiteral(input.title, output.title, "de");
  ```


* `piveau.mapTypedLiteral(source, target, type)`

  Returning a JSON-LD typed literal with the value of source and optionally with the given type. If target exist, it will
  add it as an additional triple (returning an array, which includes all previous values). If source is undefined, null
  or blank, the method returns `undefined`.
  ```javascript
  output.fictiv = piveau.mapTypedLiteral(source, output.fictiv, "xsd:boolean");
  ```


* `piveau.mapReference(source, target, type)`

  Returning a JSON-LD uriRef with the value of source and optionally with the given type. If target exist, it will
  add it as an additional triple (returning an array, which includes all previous values). If source is undefined, null
  or blank, the method returns `undefined`.
  ```javascript
  output.page = piveau.mapReference(source, output.page, "Document");
  ```


* `piveau.mapSingleLiteral(source, lang)`

  Returning a JSON-LD literal with the value of source and optionally with the given language. If source is undefined, null
  or blank, the method returns `undefined`.
  ```javascript
  output.name = piveau.mapSingleLiteral(source, "de");
  ```


* `piveau.mapSingleTypedLiteral(source, type)`

  Returning a JSON-LD typed literal with the value of source and optionally with the given type. If source is undefined, null
  or blank, the method returns `undefined`.
  ```javascript
  output.byteSize = piveau.mapSingleTypedLiteral(source, "xsd:nonNegativeInteger");
  ```


* `piveau.mapSingleReference(source, type)`

  Returning a JSON-LD typed uriRef with the value of source and optionally with the given type. If source is undefined, null
  or blank, the method returns `undefined`.
  ```javascript
  dist.rights = piveau.mapSingleReference(source, "RightsStatement");
  ```

## Debugging Scripts

Start your transforming function with `debugger`keyword. This is the entry point for your debugging session, therefore 
it is recommended to put it at the beginning of your script.

```javascript
function transforming(input) {
    debugger;
    ...
}
```
You can utilize the `PiveauScriptEngineTest.debugScript()` method to start a script debugging session with a given script and input.
When you start the test, the console output provides you with a chrome URL which you need to open in your Chrome browser.
Usually it looks like this:

```url
devtools://devtools/bundled/js_app.html?ws=127.0.0.1:4242/debug
```

Copy this URL into your Chrome browsers address field and enter. Et voilà
